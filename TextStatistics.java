import java.io.*;
import java.util.Scanner;
/*
 *класс для подсчета пробелов,символов,символов без пробелов и количесва слов в текстовом файле
 */
public class TextStatistics {
    private static Scanner scanner = new Scanner(System.in);
    final private static String GENERAL_STATISTICS_BY_TEXT =
            "Количество пробелов в тексте: %d \n" +
            "Количество символов в тексте: %d \n" +
            "Количество символов без пробела в тексте: %d \n" +
            "Количество слов в тексте: %d,";
    final private static String INFORMATION_TO_THE_USER_ON_THE_ENTRY_OF_A_FILE =
            "\n'справка для пользователя'\n\n" +
            "Введите путь до файла по примеру\n" +
            "пример:\n" +
            "D:\\CPY_SAVES\\CPY\\UPLAY\\text.txt\n" +
            "Где D: - это диск на котором находится этот текстовый файл,\n" +
            "все что идет после диска это путь к самому файлу\n" +
            "(путь к файлу можно псмотреть с помощью свойств самого фокуменав разделе 'безопастность' там будет указан полный путь к файлу\n" +
            "Расширение для файла '.txt' ставить обязательно\n" +
            "и указать обязательно кодировку для всех файлов 'UTF8' ";
    final private static String ERROR_IN_READING_FILE_WAY =
            "вы не верно указали путь к файлу или указали не верное рассширение для файла";
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(getFile()))) {

            String fileLine;
            int space = 0;
            int symbols = 0;
            int word = 0;

            while ((fileLine = bufferedReader.readLine()) != null) {
                space += countingOfSpace(fileLine);
                symbols += countingOfSymbols(fileLine);
                word += countingOfWord(fileLine);
            }
            System.out.printf(GENERAL_STATISTICS_BY_TEXT, space, symbols, symbols - space, word);
        } catch (IOException e) {
            System.out.print(ERROR_IN_READING_FILE_WAY);
        }
    }
    /**
     * Метод позволяет ввести пользователю путь к файлу,
     * потребителю предоставляется информация как правильно вводить путь к документу.
     * scanner позволяет пользователю ввести путь.
     * String будет хранить путь к файлу.
     */
    private static String getFile() {
        System.out.println(INFORMATION_TO_THE_USER_ON_THE_ENTRY_OF_A_FILE);
        return scanner.nextLine();
    }
    /**
     * Метод находит количество пробелов в предложенях.
     * Метод удаляет все кроме пробелов и считает их количество.
     */
    private static int countingOfSpace(String fileLine) {
        return fileLine.replaceAll("[^ ]", "").length();
    }
    /*
     *метод который подсчитывает количество символов в тексте
     *считая все символы, в каждом предложении
     */
    private static int countingOfSymbols(String fileLine) {
        return fileLine.length();
    }
    /*
     *метод который подсчитывает количество слов в тексте
     *заменяет  знаки препинания, символы и числа на пробелы
     * удаляя все "-" в тексте
     * и заменяя повторяющиеся пробелы в текте на один пробел
     * спомощью split() рабивает предложения на оельные слова
     */
    private static int countingOfWord(String fileLine) {
        fileLine = fileLine.replaceAll("[^-aA-zZаА-яЯ0-9 ]", " ");
        fileLine = fileLine.replaceAll("-", "");
        fileLine = fileLine.replaceAll(" {2,}", " ");
        return fileLine.split(" ").length;
    }
}
